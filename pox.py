#!/usr/bin/env python
"""lingvoj.py

Usage:
    tox.py post-update <sourcedir> <filemask>
    tox.py pre-commit <sourcedir> <filemask>

Options:
    sourcedir: Path to the source directory
"""

import os
from docopt import docopt
from translate.storage import factory
import subprocess

import logging
logging.basicConfig(level=logging.DEBUG, filename='/tmp/pox.log')

class Pox:
    def __init__(self, directory, filemask):
        self.directory = directory
        self.filemask = filemask

    def precommit(self):
        self.walk_into_dirs()
        return 0

    def postupdate(self):
        self.walk_into_dirs(True)
        return 0

    def regen_pofiles(self, generate_pot=True):
        logging.debug('Regenerating pofiles')
        _dirpath = os.path.dirname(self.filemask)
        if _dirpath == '':
            _dirpath = '.'
        for dirpath, _, filenames in os.walk('.'):
            if not os.path.relpath('.', dirpath) == os.path.relpath('.', _dirpath):
                logging.debug('%s != %s' % (os.path.relpath('.', dirpath), os.path.relpath('.', _dirpath)))
                continue
            logging.debug('Found potfiles in %s' % dirpath)
            logging.debug('Pot in %s' % dirpath)
            potfiles = self.get_pot_files(filenames)
            for potfilename in potfiles:
                orig_filename = '%s.en.xhtml' % (potfilename.split('.')[1])
                command = ['html2po',
                           os.path.join(dirpath, orig_filename),
                           '-P',
                           os.path.join(dirpath, potfilename)]
                logging.info('Running %s' % ' '.join(command))
                subprocess.call(command)
                with open(os.path.join(dirpath, potfilename)) as f:
                    lines = f.readlines()
                with open(os.path.join(dirpath, potfilename), 'w') as f:
                    changed = False
                    for line in lines:
                        if line.startswith('"POT-Creation-Date:') and not changed:
                            changed = True
                        else:
                            f.write(line)
                try:
                    command = ['git', 'add', os.path.join(dirpath, potfilename)]
                    logging.info('Running %s' % ' '.join(command))
                    subprocess.call(command)
                except:
                    pass
            if len(potfiles) > 0:
                command = ['msgcat'] + sorted([os.path.join(dirpath, x) for x in potfiles]) + ['-o', os.path.join(dirpath, 'locale.pot')]
                logging.info('Running %s' % ' '.join(command))
                subprocess.call(command)
                try:
                    command = ['git', 'add', os.path.join(dirpath, 'locale.pot')]
                    logging.info('Running %s' % ' '.join(command))
                    subprocess.call(command)
                except:
                    pass
            pofiles = self.get_po_files(filenames)
            for pofile in pofiles:
                command = ['msgmerge', '--backup=off', '-U', os.path.join(dirpath, pofile), os.path.join(dirpath, 'locale.pot')]
                logging.info('Running %s' % ' '.join(command))
                subprocess.call(command)
                try:
                    command = ['git', 'add', os.path.join(dirpath, pofile)]
                    logging.info('Running %s' % ' '.join(command))
                    subprocess.call(command)
                except:
                    pass
        logging.debug('End Of Regenerating pofiles')

    def walk_into_dirs(self, generate_pot=False):
        os.chdir(self.directory)
        if generate_pot:
            self.regen_pofiles()

        _dirpath = os.path.dirname(self.filemask)
        if _dirpath == '':
            _dirpath = '.'
        for dirpath, _, filenames in os.walk('.'):
            if not os.path.relpath('.', dirpath) == os.path.relpath('.', _dirpath):
                logging.debug('%s != %s' % (os.path.relpath('.', dirpath), os.path.relpath('.', _dirpath)))
                continue
            if 'locale.pot' in filenames:
                logging.debug('Pot in %s' % dirpath)
                pofiles = self.get_po_files(filenames)
                translated_filenames = self.extract_files_from_pot(dirpath)
                logging.debug('Filenames files: %s' % filenames)
                logging.debug('Po files: %s' % pofiles)
                for pofilename in pofiles:
                    logging.info('Analyzing %s' % pofilename)
                    pofile = factory.getobject(os.path.join(dirpath, pofilename))
                    for filename in translated_filenames:
                        logging.info('Analyzing with %s' % filename)
                        if self.is_pofile_translated_for_file(pofile, filename):
                            lang = pofilename.split('.')[1]
                            newname = '%s.%s.xhtml' % (filename.split('.')[0], lang)
                            command = ['po2html',
                                             '-t',
                                             filename,
                                             os.path.join(dirpath, pofilename),
                                             newname]
                            logging.info('Running %s' % ' '.join(command))
                            subprocess.call(command)
                            try:
                                command = ['git', 'add', newname]
                                logging.info('Running %s' % ' '.join(command))
                                subprocess.call(command)
                            except:
                                pass


    def is_pofile_translated_for_file(self, pofile, filename):
        result = False
        for unit in pofile.unit_iter():
            logging.debug("Unit %s (type: %s)" % (unit, type(unit)))
            for source in unit.sourcecomments:
                logging.debug("Source %s" % source)
                logging.debug("is blank? %s" % unit.isblank())
                logging.debug("is translated? %s" % unit.istranslated())
                logging.debug("is fuzzy? %s" % unit.isfuzzy())
                path = source[3:].split('+')[0]
                logging.debug('Comparing %s and %s' % (path, filename))
                if filename == path:
                    if unit.isblank():
                        logging.debug('Blank!')
                        return False
                    if unit.isfuzzy():
                        logging.debug('Fuzzy!')
                        return False
                    if not unit.istranslated():
                        logging.debug('Not Translated!')
                        return False
                    result = True
        return result

    def extract_files_from_pot(self, dirpath):
        filenames = set()
        with open(os.path.join(dirpath, 'locale.pot'), 'r') as f:
            for line in f:
                if line.startswith('#: '):
                    filename = line[3:].split('+')[0]
                    filenames.add(filename)
        return filenames

    def get_pot_files(self, filenames):
        results = []
        for filename in filenames:
            if not filename.startswith('.'):
                logging.debug('Filename does not start with .: %s' % filename)
                continue
            if not filename.endswith('.pot'):
                logging.debug('Filename does not ends with .pot: %s' % filename)
                continue
            if not len(filename.split('.')) == 3:
                logging.debug('Filename does not contain 2 dots: %s' % filename)
                continue
            results.append(filename)
        return results

    def get_po_files(self, filenames):
        results = []
        for filename in filenames:
            if not filename.startswith('locale.'):
                logging.debug('Filename does not start with locale.: %s' % filename)
                continue
            if not filename.endswith('.po'):
                logging.debug('Filename does not ends with .po: %s' % filename)
                continue
            if filename.endswith('.en.po'):
                logging.debug('Filename is in English: %s' % filename)
                continue
            if not len(filename.split('.')) == 3:
                logging.debug('Filename does not contain 2 dots: %s' % filename)
                continue
            if filename.split('.')[2] == 'en':
                logging.debug('Skipping en: %s' % filename)
                continue
            results.append(filename)
        return results

def main():
    arguments = docopt(__doc__, version='0.1')
    srcdir = arguments['<sourcedir>']
    filemask = arguments['<filemask>']
    if arguments['pre-commit']:
        exit(Pox(srcdir, filemask).precommit())
    elif arguments['post-update']:
        exit(Pox(srcdir, filemask).postupdate())


if __name__ == '__main__':
    main()

