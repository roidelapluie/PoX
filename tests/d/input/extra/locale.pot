#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-18 20:59+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#: extra/index.en.xhtml+html.head.title:3
msgid "This is a test"
msgstr ""

#: extra/index.en.xhtml+html.body.p:6
msgid "This is a test of the ToX project."
msgstr ""

#: extra/index.en.xhtml+html.body.p:7
msgid "This too."
msgstr ""
