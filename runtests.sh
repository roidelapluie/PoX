#!/bin/bash
set -e

results=$(mktemp)
exitcode=0

for i in tests/a tests/b tests/c tests/d;
do
    name="$(basename $i)"
    echo "-------------${name//?/-}"
    echo "Running test $name"
    echo "-------------${name//?/-}"
    echo
    tmpdir="$(mktemp -d)"
    cp -rv $i/input/ "$tmpdir"
    ./pox.py pre-commit "$tmpdir/input" "*.po"
    echo "Comparing $tmpdir/input and $i/output"
    if diff -r "$tmpdir/input" "$i/output"
    then
        echo -e "Test $(basename $i) \e[1;32mSUCCESS\e[0m" | tee -a "$results"
    else
        echo -e "Test $(basename $i) \e[1;31mFAILURE\e[0m" | tee -a "$results"
        exitcode=1
    fi
    echo
done
for i in tests/e;
do
    name="$(basename $i)"
    echo "-------------${name//?/-}"
    echo "Running test $name"
    echo "-------------${name//?/-}"
    echo
    tmpdir="$(mktemp -d)"
    cp -rv $i/input/ "$tmpdir"
    ./pox.py post-update "$tmpdir/input" "*.po"
    echo "Comparing $tmpdir/input and $i/output"
    if diff -I Language:.fr -I POT-Creation-Date -ur "$tmpdir/input" "$i/output"
    then
        echo -e "Test $(basename $i) \e[1;32mSUCCESS\e[0m" | tee -a "$results"
    else
        echo -e "Test $(basename $i) \e[1;31mFAILURE\e[0m" | tee -a "$results"
        exitcode=1
    fi
    echo
done
echo "------------"
echo "Test Results"
echo "------------"
echo
cat "$results"


exit $exitcode
